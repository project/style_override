$Id

Style Override
===============
Allows administrators to identify css selectors and properties, which are 
applied based on the result of rules.


-- REQUIREMENTS --

Color picker.  Available at http://www.eyecon.ro/colorpicker.  
See below for installation instructions.

jQuery Update 6.x-2.x: jQuery 1.3.x

jQuery UI 6.x-1.x: Updated to include jQuery UI 1.7

Permission to edit theme.

-- INSTALLATION --

1)  Add color picker to the module directory:
Download the jQuery color picker script 
(http://www.eyecon.ro/colorpicker/#download) and unpack into this directory 
(ie. yoursite/sites/all/modules/style_override/colorpicker).

2)  Make sure that you have version 6.x-2.x or later of the jQuery Update module. 

3)  Update the jQuery UI module so that it uses jQuery UI 1.7. See the jQuery UI's README file for instructions on how to do this.

4)  Add <?php print $style_override ?> to your theme's page.tpl.php file directly after <?php print $styles ?>  

You will have to add this to all page template files. For example if you have a front.page.tpl.php template, add <?php print $style_override ?> here as well.

5)  Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION AND USAGE --

1)  Configure user permissions in admin/user/permissions

Any user with the administer style override module permission will be able to add rules and selectors and modify CSS properties using the module.

2)  Add rule on admin/settings/style_override/add. Give the rule a name and  set the visibility the same way that you would for a block.

3)  Add selectors though the "Manage Selectors" link next to the rule name. Give the selector a title and selector value. For example "Title: Site Name" and "Selector: h1#site-name a"

4)  Now visit a page to which the rule applies. You should have a style editing form that you can expand at the bottom of the page. Edit the styles and save them.

-- AUTHOR/MAINTAINER --

Maureen Lyons (maureen)
maureen underscore lyons at mac dot com
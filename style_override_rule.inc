<?php

/**
 * @file
 * Administrative pages for the module.
 *
 * Contains form building functions, submit handlers, and theme functions for
 * the module's overview form, add and edit forms, and the delete confirmation
 * form.
 */

/**
 * Build an overview form with drag and drop re-ordering of rules.
 *
 * @ingroup forms
 * @see _style_override_rule_overview_rule_field()
 * @see style_override_rule_overview_form_submit()
 * @see theme_style_override_rule_overview_form()
 * @see style_override_rule_overview_pager()
 */
function style_override_rule_overview_form(&$form_state) {
  $rules = style_override_rule_load_all();

  $form['rules']['#tree'] = TRUE;
  foreach ($rules as $rule_id => $rule) {
    $form['rules'][$rule_id] = _style_override_rule_overview_rule_field($rule);
  }

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
    '#disabled' => empty($rules),
  );

  return $form;
}

/**
 * Builds the fields for a single rule on the drag-and-drop overview form.
 *
 * @ingroup forms
 * @see style_override_rule_overview_form()
 */
function _style_override_rule_overview_rule_field($rule) {
  $form['rule_id'] = array(
    '#type' => 'hidden',
    '#value' => $rule['rule_id'],
  );

  $form['title'] = array(
    '#type' => 'markup',
    '#value' => l($rule['title'], 'admin/settings/style_override/'. $rule['rule_id']),
  );

  $form['weight'] = array(
    '#type' => 'weight',
    '#default_value' => $rule['weight'],
  );

  $form['operations'] = array(
    '#type' => 'markup',
    '#value' => _style_override_rule_links($rule),
  );

  return $form;
}

/**
 * Build the edit and delete links for a single rule.
 *
 * @see style_override_rule_overview_form()
 * @see style_override_rule_overview_pager()
 */
function _style_override_rule_links($rule) {
  $image_rule = drupal_get_path('module', 'style_override') .'/images/';
  $links['manage_selectors'] = array(
    'title' => t('Manage Selectors'),
    'href' => 'admin/settings/style_override/'. $rule['rule_id'],
    'html' => TRUE,
    'query' => drupal_get_destination(),
  );
  $links['edit'] = array(
    'title' => theme('image', $image_rule .'text-editor.png', t('Edit Rule')),
    'href' => 'admin/settings/style_override/'. $rule['rule_id'] .'/edit',
    'html' => TRUE,
    'query' => drupal_get_destination(),
  );
  $links['delete'] = array(
    'title' => theme('image', $image_rule .'edit-delete.png', t('Delete Rule')),
    'href' => 'admin/settings/style_override/'. $rule['rule_id'] .'/delete',
    'html' => TRUE,
    'query' => drupal_get_destination(),
  );
  return theme('links', $links);
}

/**
 * General submit handler for the drag-and-drop overview form.
 *
 * Updates the weights of all rules on the form.
 *
 * @ingroup formapi
 * @see style_override_rule_overview_form()
 */
function style_override_rule_overview_form_submit($form, &$form_state) {
  $rules = $form_state['values']['rules'];
  foreach ($rules as $rule) {
    style_override_rule_save($rule);
  }
}

/**
 * Theme the drag-and-drop overview form.
 *
 * Arranges rules in a table, and adds the css and js for draggable sorting.
 *
 * @ingroup themeable
 * @ingroup forms
 * @see style_override_rule_overview_form()
 */
function theme_style_override_rule_overview_form($form) {
  // Each rule has a 'weight' that can be used to arrange it in relation to
  // other rules. Drupal's tabledrag.js library allows users to control these
  // weights by dragging and dropping the rules in a list -- we just need to
  // add identifying CSS classes to key elements in the table.

  $rows = array();
  foreach (element_children($form['rules']) as $key) {
    $row = array();

    // Render the hidden 'rule id' field and the title of the rule into the
    // same column of the row.
    $row[] = drupal_render($form['rules'][$key]['rule_id']) . drupal_render($form['rules'][$key]['title']);

    // Add an identifying CSS class to our weight field, as it's the one
    // the tabledrag.js will be controlling. This can be anything we want it to
    // be, we'll just tell the tabledrag.js library what it should look for.
    $form['rules'][$key]['weight']['#attributes']['class'] = 'style-override-weight';
    $row[] = drupal_render($form['rules'][$key]['weight']);

    // Render the edit and delete links into their own column.
    $row[] = drupal_render($form['rules'][$key]['operations']);

    // Add the new row to our collection of rows, and give it the 'draggable'
    // class, indicating that it should be... well, draggable.
    $rows[] = array(
      'data' => $row,
      'class' => 'draggable',
    );
  }

  // If there were no rules found, note the fact so users don't get confused
  // by a completely empty table.
  if (count($rows) == 0) {
    $rows[] = array(t('No rules have been added.'), '<span class="style-override-weight"></span>', '');
  }

  // Render a list of header titles, and our array of rows, into a table. Even
  // we've already rendered all of our rules, we always call drupal_render()
  // on the form itself after we're done, so hidden security fields and other
  // elements (like buttons) will appear properly at the bottom of the form.
  $header = array(t('Rule'), t('Weight'), t('Operations'));
  $output = theme('table', $header, $rows, array('id' => 'style-override-overview'));
  $output .= drupal_render($form);

  // Now that we've built our output, tell Drupal to add the tabledrag.js library.
  // We'll pass in the ID of the table, the behavior we want it to use, and the
  // class that appears on each 'weight' form element it should be controlling.
  drupal_add_tabledrag('style-override-overview', 'order', 'self', 'style-override-weight');

  return $output;
}

/**
 * Build the rule editing form.
 *
 * If a rule is passed in, an edit form with both Save and Delete buttons will
 * be built. Otherwise, a blank 'add new rule' form, without the Delete button,
 * will be built.
 *
 * @ingroup forms
 * @see style_override_rule_form_submit()
 * @see style_override_rule_form_delete()
 */
function style_override_rule_form(&$form_state, $rule = array()) {
  // Set the default values for a new item. By using += rather than =, we
  // only overwrite array keys that have not yet been set. It's safe to use
  // on both an empty array, and an incoming array with full or partial data.
  $rule += array(
    'title' => '',
    'content' => '',
    'weight' => 0,
  );

  // If we're editing an existing rule, we'll add a value field to the form
  // containing the rule's unique ID.
  if (!empty($rule['rule_id'])) {
    $form['rule_id'] = array(
      '#type' => 'value',
      '#value' => $rule['rule_id'],
    );
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Rule'),
    '#required' => TRUE,
    '#default_value' => $rule['title'],
  );

  // Shamelessly ripped from block.module. Who doesn't use this snippet
  // of code, really?
  $access = user_access('use PHP for block visibility');
  if ($rule['rule_type'] == 2 && !$access) {
    $form['conditional'] = array();
    $form['conditional']['rule_type'] = array('#type' => 'value', '#value' => 2);
    $form['conditional']['rule_conditions'] = array('#type' => 'value', '#value' => $rule['rule_conitions']);
  }
  else {
    $options = array(t('Add on every page except the listed pages.'), t('Add on only the listed pages.'));
    $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array('%blog' => 'blog', '%blog-wildcard' => 'blog/*', '%front' => '<front>'));

    if ($access) {
      $options[] = t('Add if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
      $description .= ' '. t('If the PHP-mode is chosen, enter PHP code between %php. Note that executing incorrect PHP-code can break your Drupal site.', array('%php' => '<?php ?>'));
    }
    $form['conditional']['rule_type'] = array(
      '#type' => 'radios',
      '#title' => t('Add the CSS on specific pages'),
      '#options' => $options,
      '#default_value' => $rule['rule_type'],
    );
    $form['conditional']['rule_conditions'] = array(
      '#type' => 'textarea',
      '#title' => t('Pages'),
      '#default_value' => $rule['rule_conditions'],
      '#description' => $description,
    );
  }

  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $rule['weight'],
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  // Only show the delete button if we already have an ID. Set the delete
  // button's submit handler to a custom function that should only fire if
  // this button is clicked. In all other cases, the form will fall back to
  // the default $form_id_submit() function.
  if (!empty($rule['rule_id'])) {
    $form['buttons']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('style_override_rule_form_delete'),
    );
  }

  return $form;
}

/**
 * General submit handler for Scaffolding's add/edit form.
 *
 * Simply passes incoming form values on to the module's CRUD save function,
 * then redirects to the overview form.
 *
 * @ingroup formapi
 * @see style_override_rule_form()
 */
function style_override_rule_form_submit($form, &$form_state) {
  $rule = $form_state['values'];
  style_override_rule_save($rule);
  $form_state['redirect'] = 'admin/settings/style_override';
}

/**
 * Delete button submit handler for Scaffolding's add/edit form.
 *
 * Redirects to the 'delete rule' confirmation page without performing any
 * operations.
 *
 * @ingroup formapi
 * @see style_override_rule_form()
 */
function style_override_rule_form_delete($form, &$form_state) {
  $form_state['redirect'] = 'admin/settings/style_override/'. $form_state['values']['rule_id'] .'/delete';
}

/**
 * Build the delete confirmation form.
 *
 * A simple wrapper around Drupal's core confirm_form() function. Adds a value
 * field to store the ID of the rule being deleted.
 *
 * @ingroup forms
 * @see style_override_delete_confirm_submit()
 * @see confirm_form()
 */
function style_override_rule_delete_confirm(&$form_state, $rule) {
  $form['rule_id'] = array(
    '#type' => 'value',
    '#value' => $rule['rule_id'],
  );

  return confirm_form($form,
    t('Are you sure you want to delete %rule?', array('%rule' => $rule['title'])),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/style_override',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * General submit handler for the delete confirmation form.
 *
 * Core's confirm_form() function adds the 'confirm' value element we check
 * against to ensure the form was properly submitted. If it's there, delete
 * the rule and redirect to the overview form.
 *
 * @ingroup formapi
 * @see style_override_rule_form()
 */

function style_override_rule_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    style_override_rule_delete($form_state['values']['rule_id']);
    drupal_set_message(t('Your rule was deleted.'));
  }
  $form_state['redirect'] = 'admin/settings/style_override';
}

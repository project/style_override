<?php

/**
 * @file
 * Administrative pages for the module.
 *
 * Contains form building functions, submit handlers, and theme functions for
 * the module's overview form, add and edit forms, and the delete confirmation
 * form.
 */

/**
 * Build an overview form with drag and drop re-ordering of selectors.
 *
 * Loads all selectors and builds an overview form with weight elements for each
 * selector, then adds Drupal's tabledrag.js file. Because ALL selectors will be
 * displayed on this page, this style of overview is best suited to small sets
 * of data.
 *
 * @ingroup forms
 * @see _style_override_overview_selector_field()
 * @see style_override_overview_form_submit()
 * @see theme_style_override_overview_form()
 */
function style_override_selector_overview_form(&$form_state) {
  $rule_id = arg(3);
  $rule = style_override_rule_load($rule_id);
  $selectors = style_override_selector_load_by_rule($rule_id);

  $form['selectors'] = array(
    '#tree' => TRUE,
  );

  foreach ($selectors as $selector_id => $selector) {
    $form['selectors'][$selector_id] = _style_override_selector_overview_field($selector);
  }

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
    '#disabled' => empty($selectors),
  );

  return $form;
}

/**
 * Builds the fields for a single selector on the drag-and-drop overview form.
 *
 * @ingroup forms
 * @see style_override_overview_form()
 */
function _style_override_selector_overview_field($selector) {
  $form['selector_id'] = array(
    '#type' => 'hidden',
    '#value' => $selector['selector_id'],
  );

  $form['rule_id'] = array(
    '#type' => 'hidden',
    '#value' => arg(3),
  );

  $form['title'] = array(
    '#type' => 'markup',
    '#value' => check_plain($selector['title']),
  );

  $form['selector'] = array(
    '#type' => 'markup',
    '#value' => check_plain($selector['selector']),
  );

  $form['weight'] = array(
    '#type' => 'weight',
    '#default_value' => $selector['weight'],
  );

  $form['operations'] = array(
    '#type' => 'markup',
    '#value' => _style_override_selector_links($selector),
  );

  return $form;
}

/**
 * Build the edit and delete links for a single selector.
 *
 * @see style_override_overview_form()
 */
function _style_override_selector_links($selector) {
  $path = drupal_get_path('module', 'style_override') .'/images/';
  $links['edit'] = array(
    'title' => theme('image', $path .'text-editor.png', t('Edit')),
    'href' => 'admin/settings/style_override/'. arg(3) .'/'. $selector['selector_id'] .'/edit',
    'html' => TRUE,
    'query' => drupal_get_destination(),
  );
  $links['delete'] = array(
    'title' => theme('image', $path .'edit-delete.png', t('Delete')),
    'href' => 'admin/settings/style_override/'. arg(3) .'/'. $selector['selector_id'] .'/delete',
    'html' => TRUE,
    'query' => drupal_get_destination(),
  );
  return theme('links', $links);
}

/**
 * General submit handler for the drag-and-drop overview form.
 *
 * Updates the weights of all selectors on the form.
 *
 * @ingroup formapi
 * @see style_override_overview_form()
 */
function style_override_selector_overview_form_submit($form, &$form_state) {
  $selectors = $form_state['values']['selectors'];
  foreach ($selectors as $selector) {
    style_override_selector_save($selector);
  }
}

/**
 * Theme the drag-and-drop overview form.
 *
 * Arranges selectors in a table, and adds the css and js for draggable sorting.
 *
 * @ingroup themeable
 * @ingroup forms
 * @see style_override_overview_form()
 */
function theme_style_override_selector_overview_form($form) {
  // Each selector has a 'weight' that can be used to arrange it in relation to
  // other selectors. Drupal's tabledrag.js library allows users to control these
  // weights by dragging and dropping the selectors in a list -- we just need to
  // add identifying CSS classes to key elements in the table.

  $rows = array();
  foreach (element_children($form['selectors']) as $key) {
    $row = array();

    // Render the hidden 'selector id' field and the title of the selector into the
    // same column of the row
    $row[] = drupal_render($form['selectors'][$key]['selector_id']) .  drupal_render($form['selectors'][$key]['rule_id']) . drupal_render($form['selectors'][$key]['title']);

    // Render the actual selector into the row
    $row[] = drupal_render($form['selectors'][$key]['selector']);

    // Add an identifying CSS class to our weight field, as it's the one
    // the tabledrag.js will be controlling. This can be anything we want it to
    // be, we'll just tell the tabledrag.js library what it should look for.
    $form['selectors'][$key]['weight']['#attributes']['class'] = 'style-override-weight';
    $row[] = drupal_render($form['selectors'][$key]['weight']);

    // Render the edit and delete links into their own column.
    $row[] = drupal_render($form['selectors'][$key]['operations']);

    // Add the new row to our collection of rows, and give it the 'draggable'
    // class, indicating that it should be... well, draggable.
    $rows[] = array(
      'data' => $row,
      'class' => 'draggable',
    );
  }

  // If there were no selectors found, note the fact so users don't get confused
  // by a completely empty table.
  if (count($rows) == 0) {
    $rows[] = array(t('No selectors have been added.'), '<span class="style-override-weight"></span>', '');
  }

  // Render a list of header titles, and our array of rows, into a table. Even
  // we've already rendered all of our selectors, we always call drupal_render()
  // on the form itself after we're done, so hidden security fields and other
  // elements (like buttons) will appear properly at the bottom of the form.
  $header = array(t('Title'), t('Selector'), t('Weight'), t('Operations'));
  $output = theme('table', $header, $rows, array('id' => 'style-override-overview'));
  $output .= drupal_render($form);

  // Now that we've built our output, tell Drupal to add the tabledrag.js library.
  // We'll pass in the ID of the table, the behavior we want it to use, and the
  // class that appears on each 'weight' form element it should be controlling.
  drupal_add_tabledrag('style-override-overview', 'order', 'self', 'style-override-weight');

  return $output;
}


/**
 * Build the selector editing form.
 *
 * If a selector is passed in, an edit form with both Save and Delete buttons will
 * be built. Otherwise, a blank 'add new selector' form, without the Delete button,
 * will be built.
 *
 * @ingroup forms
 * @see style_override_form_submit()
 * @see style_override_form_delete()
 */
function style_override_selector_form(&$form_state, $selector = array()) {
  // Set the default values for a new item. By using += rather than =, we
  // only overwrite array keys that have not yet been set. It's safe to use
  // on both an empty array, and an incoming array with full or partial data.
  $selector += array(
    'title' => '',
    'weight' => 0,
  );

  // If we're editing an existing selector, we'll add a value field to the form
  // containing the selector's unique ID.
  if (!empty($selector['selector_id'])) {
    $form['selector_id'] = array(
      '#type' => 'value',
      '#value' => $selector['selector_id'],
    );
  }

  $form['rule_id'] = array(
    '#type' => 'hidden',
    '#value' => arg(3),
  );

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#description' => t('This is the name that will appear on the style editing form.'),
    '#default_value' => $selector['title'],
  );

  $form['selector'] = array(
    '#type' => 'textfield',
    '#title' => t('Selector'),
    '#required' => TRUE,
    '#default_value' => $selector['selector'],
  );

  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $selector['weight'],
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  // Only show the delete button if we already have an ID. Set the delete
  // button's submit handler to a custom function that should only fire if
  // this button is clicked. In all other cases, the form will fall back to
  // the default $form_id_submit() function.
  if (!empty($selector['selector_id'])) {
    $form['buttons']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#submit' => array('style_override_form_delete'),
    );
  }

  return $form;
}

/**
 * General submit handler for Selector's add/edit form.
 *
 * Simply passes incoming form values on to the module's CRUD save function,
 * then redirects to the overview form.
 *
 * @ingroup formapi
 * @see style_override_form()
 */
function style_override_selector_form_submit($form, &$form_state) {
  $selector = $form_state['values'];
  style_override_selector_save($selector);
  $form_state['redirect'] = 'admin/settings/style_override/'. $form_state['values']['rule_id'];
}

/**
 * Delete button submit handler for Scaffolding's add/edit form.
 *
 * Redirects to the 'delete selector' confirmation page without performing any
 * operations.
 *
 * @ingroup formapi
 * @see style_override_form()
 */
function style_override_selector_form_delete($form, &$form_state) {
  $form_state['redirect'] = 'admin/settings/style_override/rule/'. $form_state['values']['selector_id'] .'/delete';
}

/**
 * Build the delete confirmation form.
 *
 * A simple wrapper around Drupal's core confirm_form() function. Adds a value
 * field to store the ID of the selector being deleted.
 *
 * @ingroup forms
 * @see style_override_delete_confirm_submit()
 * @see confirm_form()
 */
function style_override_selector_delete_confirm(&$form_state, $selector) {
  $form['selector_id'] = array(
    '#type' => 'value',
    '#value' => $selector['selector_id'],
  );

  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $selector['selector'])),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/settings/style_override/rule',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * General submit handler for the delete confirmation form.
 *
 * Core's confirm_form() function adds the 'confirm' value element we check
 * against to ensure the form was properly submitted. If it's there, delete
 * the selector and redirect to the overview form.
 *
 * @ingroup formapi
 * @see style_override_form()
 */

function style_override_selector_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    style_override_selector_delete($form_state['values']['selector_id']);
    drupal_set_message(t('Your selector was deleted.'));
  }
  $form_state['redirect'] = 'admin/settings/style_override/rule';
}


$(document).ready(function() {

$('.ui-tabs').tabs();

$('#style-override-properties-form').hide();

$('body').css({'padding-bottom':'100px'});
$('#style_override h2').toggle(
  function () {
    $('body').css({'padding-bottom':'370px'});
    $('#style-override-properties-form').show();
  },
  function () {
    $('body').css({'padding-bottom':'100px'});
    $('#style-override-properties-form').hide();
  }
);

// sort selectors object and method
// many thanks to Graham Bradley at http://gbradley.co.uk

var selector={
  specificity:function(s){    // accepts a single selector
    s=s.replace(/\([^\)]+\)/,"");
    return parseInt([
        /#[\d\w-_]+/g,
        /[\.:\[][^\.:\[+>]+/g,
        /(^|[\s\+>])\w+/g
        ].map(function(p){
      var m=s.match(p);
      return m ? m.length.toString(16) : 0;
      }).join(''),16);
    },
  sort:function(s){      // accepts an array of selectors
    return a.sort(function(a,b){
      a=selector.specificity(a), b=selector.specificity(b);
      return a < b ? 1 : (a > b) ? -1 : 0;
    });
  }
};

// create an array of the selectors and sort them 

var a=[];
jQuery.each(Drupal.settings.selectors, function(selectors, selector) {
  a.push(selector.selector);
});
selector.sort(a);

// create an object to hold selectors and their keys

var keys = new Object;
jQuery.each(Drupal.settings.selectors, function(selectors, selector) {
  keys[selector.selector] = selector.selector_id;
});

// function to exculde elements from style changes

function excludeElements(element, a, keys, property) {
  var not = "";
  var index = jQuery.inArray(element, a);
  jQuery.each(a, function(i, val) {
    var key = keys[val];
    if((i<index) && $('#edit-properties-' + key + '-' + property).val()) {
      not = not + ':not(' + val + ')';
    }
  });
  var arr = element.split(" ");
  var last = (arr.pop());
  not = not = ':not(#style_override ' + last + '):not(#simplemenu ' + last + ')';
  return not;
}

// reset the form on reload and load saved styles

function start() {
  $('#style-override-properties-form')[ 0 ].reset();
  a = a.reverse();
  jQuery.each(a, function() {
    var i = keys[this];
    var element = Drupal.settings.selectors[i].selector;
    jQuery.each(Drupal.settings.selectors[i].properties, function(property, propertyValues) {
      if(propertyValues.value) {
        var arr = element.split(" ");
        var last = (arr.pop());
        var not = ':not(#style_override ' + last + '):not(#simplemenu ' + last + ')';
        var property = property.replace(/_/, "-");
        if (property == "color") {
          $(element + not).css('color', '#' + propertyValues.value);
        } else if (property == "background-color") {
          $(element + not).css('background-color', '#' + propertyValues.value);
        } else if (property == "background-image") {
          $(element + not).css('background-image', 'url(' + propertyValues.value + ')');
        } else {
		  $(element + not).css(property, propertyValues.value);	    
	    }
      }
    });
  });
  a = a.reverse();
}
start();

function loadFormValues() {
  a = a.reverse();
  jQuery.each(a, function() {
    var i = keys[this];
    var selector = this.toString();
    var arr = selector.split(" ");
    var last = (arr.pop());
    var not = ':not(#style_override ' + last + '):not(#simplemenu ' + last + ')';
    $('#style-override-properties-form #selector_' + i + ' :input[value]').each(function () {
      var value = $(this).val();
      var property = $(this).attr('property');
      if (property == "color") {
        $(selector + not).css(property, '#' + value);
      } else if (property == "background-color") {
        $(selector + not).css(property, '#' + value);
      } else if (property == "background-image") {
        $(selector + not).css(property, 'url(' + value + ')');
      } else {
        $(selector + not).css(property, value);
      }
    });
  });
  a = a.reverse();
}
 
jQuery.each(Drupal.settings.selectors, function(selectors, selector) {
  var element = selector.selector;
  var elementId = selector.selector_id;
  var sides = ['top', 'right', 'bottom', 'left'];

// builds font-size sliders

  $('.' + elementId + '_font_size_slider').slider({
    min: 90,
    max: 900,
    slide: function(event, ui) {
      var not = excludeElements(element, a, keys, "font-size");
      $('#edit-properties-' + elementId + '-font-size').val(ui.value + '%');
      $(element + not).css('font-size', ui.value + '%');
    }
  });
  if(selector.properties.font_size) {
    var startFontSize = selector.properties.font_size.value;
    $('.' + elementId + '_font_size_slider').slider('option', 'value', parseFloat(startFontSize));
  }
  $('#edit-properties-' + elementId + '-font-size').blur(function(){
    var not = excludeElements(element, a, keys, "font-size");
    var blurFontSize = $('#edit-properties-' + elementId + '-font-size').attr('value');
    if (blurFontSize) {
      $('.' + elementId + '_font_size_slider').slider('value', parseFloat(blurFontSize));
      $(element + not).css('font-size', blurFontSize);
    } else {
      $('.' + elementId + '_font_size_slider').slider('value', 0);
      $(element + not).css('font-size', '');
      loadFormValues();
    }
  });

// sets text-align on change

  $('#edit-properties-' + elementId + '-text-align').change(function () {
    var not = excludeElements(element, a, keys, "text-align");
    $(element + not).css('text-align', $(this).attr('value'));
    if ($(this).attr('value') == '') {
      loadFormValues();
    }
  });

  // sets font-family on change

  $('#edit-properties-' + elementId + '-font-family').change(function () {
    var not = excludeElements(element, a, keys, "font-family");
    $(element + not).css('font-family', $(this).attr('value'));
    if ($(this).attr('value') == '') {
      loadFormValues();
    }
  });

  // builds line-height sliders

  $('.' + elementId + '_line_height_slider').slider({
    min: 100,
    max: 500,
    slide: function(event, ui) {
      var not = excludeElements(element, a, keys, "line-height");
      $('#edit-properties-' + elementId + '-line-height').val(ui.value + '%');
      $(element + not).css('line-height', ui.value + '%');
    }
  });
  if(selector.properties.line_height) {
    var startLineHeight = selector.properties.line_height.value;
    $('.' + elementId + '_font_size_slider').slider('option', 'value', parseFloat(startLineHeight));
  }
  $('#edit-properties-' + elementId + '-line-height').blur(function(){
    var not = excludeElements(element, a, keys, "line-height");
    var blurLineHeight = $('#edit-properties-' + elementId + '-line-height').attr('value');
    if (blurLineHeight) {
      $('.' + elementId + '_line_height_slider').slider('value', parseFloat(line_height_size));
      $(element + not).css('line-height', blurLineHeight);
    } else {
      $('.' + elementId + '_line_height_slider').slider('value', 0);
      $(element + not).css('line-height', '');
      loadFormValues();
    }
  });

  // sets font-weight on change

  $('#edit-properties-' + elementId + '-font-weight').change(function () {
    var not = excludeElements(element, a, keys, "font-weight");
    $(element + not).css('font-weight', $(this).attr('value'));
    if ($(this).attr('value') == '') {
      loadFormValues();
    }
  });

  // sets text-tranform on change

  $('#edit-properties-' + elementId + '-text-transform').change(function () {
    var not = excludeElements(element, a, keys, "text-transform");
    $(element + not).css('text-transform', $(this).attr('value'));
    if ($(this).attr('value') == '') {
      loadFormValues();
    }
  });

  // adds ColorPicker to color fields
  
  $('#edit-properties-' + elementId + '-color').ColorPicker({
    onSubmit: function(hsb, hex, rgb, el) {
      $(el).val(hex);
      $(el).ColorPickerHide();
      var not = excludeElements(element, a, keys, "color");
      $(element + not).css('color', '#' + hex);
    },
    onBeforeShow: function () {
      $(this).ColorPickerSetColor(this.value);
      var not = excludeElements(element, a, keys, "color");
      $(element + not).css('color', '#' + this.value);
    }
  })
  .bind('keyup', function(){
      $(this).ColorPickerSetColor(this.value);
      var not = excludeElements(element, a, keys, "color");
      $(element + not).css('color', '#' + this.value);
  });
  $('#edit-properties-' + elementId + '-color').blur(function(){
    var not = excludeElements(element, a, keys, "color");
    var blurColor = $('#edit-properties-' + elementId + '-color').attr('value');
    if (blurColor) {
      $(element + not).css('color', blurColor);
    } else {
      $(element + not).css('color', '');
      loadFormValues();
    }
  });

  // adds ColorPicker to background-color fields
  
  $('#edit-properties-' + elementId + '-background-color').ColorPicker({
    onSubmit: function(hsb, hex, rgb, el) {
      $(el).val(hex);
      $(el).ColorPickerHide();
      var not = excludeElements(element, a, keys, "background-color");
      $(element + not).css('background-color', '#' + hex);
    },
    onBeforeShow: function () {
      $(this).ColorPickerSetColor(this.value);
      var not = excludeElements(element, a, keys, "background-color");
      $(element + not).css('background-color', '#' + this.value);
    }
  })
  .bind('keyup', function(){
      $(this).ColorPickerSetColor(this.value);
      var not = excludeElements(element, a, keys, "background-color");
      $(element + not).css('background-color', '#' + this.value);
  });
  $('#edit-properties-' + elementId + '-background-color').blur(function(){
    var not = excludeElements(element, a, keys, "background-color");
    var blurBackgroundColor = $('#edit-properties-' + elementId + '-background-color').attr('value');
    if (blurBackgroundColor) {
      $(element + not).css('background-color', blurBackgroundColor);
    } else {
      $(element + not).css('background-color', '');
      loadFormValues();
    }
  });

  // sets background-image on change

  $('#edit-properties-' + elementId + '-background-image').blur(function () {
    var not = excludeElements(element, a, keys, "background-image");
    $(element + not).css('background-image', 'url(' + $(this).attr('value') + ')');
    if ($(this).attr('value') == '') {
      loadFormValues();
    }
  });  

  // sets background-repeat property on change

  $('#edit-properties-' + elementId + '-background-repeat').change(function () {
    var not = excludeElements(element, a, keys, "background-repeat");
    $(element + not).css('background-repeat', $(this).attr('value'));
    if ($(this).attr('value') == '') {
      loadFormValues();
    }
  });

  // sets background-position property on change

  $('#edit-properties-' + elementId + '-background-position').change(function () {
    var not = excludeElements(element, a, keys, "background-position");
    $(element + not).css('background-position', $(this).attr('value'));
    if ($(this).attr('value') == '') {
      loadFormValues();
    }
  });

  // sets display property on change

  $('#edit-properties-' + elementId + '-display').change(function () {
    var not = excludeElements(element, a, keys, "display");
    $(element + not).css('display', $(this).attr('value'));
    if ($(this).attr('value') == '') {
      loadFormValues();
    }
  });

  // builds height sliders

  $('.' + elementId + '_height_slider').slider({
    min: 0,
    max: 900,
    slide: function(event, ui) {
      $('#edit-properties-' + elementId + '-height').val(ui.value + 'px');
      var not = excludeElements(element, a, keys, "height");
      $(element + not).css('height', ui.value + 'px');
    }
  });
  if(selector.properties.height) {
    var startHeight = selector.properties.height.value;
    $('.' + elementId + '_height_slider').slider('option', 'value', parseFloat(startHeight));
  }
  $('#edit-properties-' + elementId + '-height').blur(function(){
    var not = excludeElements(element, a, keys, "height");
    var blurHeight = $('#edit-properties-' + elementId + '-height').attr('value');
    if (blurHeight) {
      $('.' + elementId + '_height_slider').slider('value', parseFloat(blurHeight));
      $(element + not).css('height', blurHeight);
    } else {
      $('.' + elementId + '_height_slider').slider('value', 0);
      $(element + not).css('height', '');
      loadFormValues();
    }
  });

  // builds width sliders

  $('.' + elementId + '_width_slider').slider({
    min: 0,
    max: 900,
    slide: function(event, ui) {
      $('#edit-properties-' + elementId + '-width').val(ui.value + 'px');
      var not = excludeElements(element, a, keys, "width");
      $(element + not).css('width', ui.value + 'px');
    }
  });
  if(selector.properties.width) {
    var startWidth = selector.properties.width.value;
    $('.' + elementId + '_width_slider').slider('option', 'value', parseFloat(startWidth));
  }
  $('#edit-properties-' + elementId + '-width').blur(function(){
    var not = excludeElements(element, a, keys, "width");
    var blurWidth = $('#edit-properties-' + elementId + '-width').attr('value');
    if (blurWidth) {
      $('.' + elementId + '_width_slider').slider('value', parseFloat(blurWidth));
      $(element + not).css('width', blurWidth);
    } else {
      $('.' + elementId + '_width_slider').slider('value', 0);
      $(element + not).css('width', '');
      loadFormValues();
    }
  });
  
  // builds padding sliders for each side

  jQuery.each(sides, function() {
    var side = this;
    $('.' + elementId + '_padding_' + side + '_slider').slider({
      min: 0,
      max: 1000,
      slide: function(event, ui) {
        $('#edit-properties-' + elementId + '-padding-' + side).val(ui.value + 'px');
        var value = $('.' + elementId + '_padding_' + side + '_slider').slider('value');
        var not = excludeElements(element, a, keys, "padding-" + side);
        $(element + not).css('padding-' + side, value + 'px');
      }
    });
    if(selector.properties['padding_' + side] ) {
      var startPadding = selector.properties['padding_' + side].value;
      $('.' + elementId + '_padding_' + side + '_slider').slider('option', 'value', parseFloat(startPadding));
    }
    $('#edit-properties-' + elementId + '-padding-' + side).blur(function(){
      var not = excludeElements(element, a, keys, "padding-" + side);
      var blurPadding = parseFloat($('#edit-properties-' + elementId + '-padding-' + side).attr('value'));
      if (blurPadding) {
        $('.' + elementId + '_padding_' + side + '_slider').slider('value', blurPadding);
        $(element + not).css('padding-' + side, blurPadding + 'px');
      } else {
        $('.' + elementId + '_padding_' + side + '_slider').slider('value', 0);
        $(element + not).css('padding-' + side, '');
        loadFormValues();
      }
    });
  });

});


});
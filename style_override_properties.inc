<?php

/**
 * @file
 * User form for style override.
 *
 * Contains form building functions and submit handlers
 */

 /**
 * Builds the end-user style editing form based on the selectors choosen in the admin form
 */

function _style_override_properties_data() {
  $selectors_by_rules = array();
  $rules = style_override_rule_load_all();
  foreach ($rules as $rule) {
    if (_style_override_evaluate_rule($rule)) {
      $selectors_by_rules[$rule['rule_id']] = style_override_selector_load_by_rule($rule['rule_id']);
    }
  }
  $properties = style_override_property_load_all();

  $data = array();

  foreach ($selectors_by_rules as $selectors) {
    foreach ($selectors as $selector_id => $selector) {
      $data[$selector_id]['selector'] = $selector['selector'];
      $data[$selector_id]['selector_id'] = $selector_id;
      $data[$selector_id]['properties'][''][''] = "";
    }
    if (is_array($properties)) {
      foreach ($properties as $property_id => $property) {
        $local_selector = $property['selector_id'];
        $local_property_name = $property['property'];
        if (array_key_exists($local_selector, $data)) {
          $data[$local_selector]['selector_id'] = $local_selector;
          $data[$local_selector]['properties'][$local_property_name]['name'] = $local_property_name;
          $data[$local_selector]['properties'][$local_property_name]['value'] = $property['value'];
        }
      }
    }
  }
  return $data;
}

function _style_override_properties_render() {
  $selectors_by_rules = array();
  $rules = style_override_rule_load_all();
  foreach ($rules as $rule) {
    if (_style_override_evaluate_rule($rule)) {
      $selectors_by_rules[$rule['rule_id']] = style_override_selector_load_by_rule($rule['rule_id']);
    }
  }
  $properties = style_override_property_load_all();

  $data = array();

  foreach ($selectors_by_rules as $selectors) {
    foreach ($selectors as $selector_id => $selector) {
      $data[$selector_id]['selector'] = $selector['selector'];
      $data[$selector_id]['selector_id'] = $selector_id;
    }
    if (is_array($properties)) {
      foreach ($properties as $property_id => $property) {
        $local_selector = $property['selector_id'];
        $local_property_name = $property['property'];
        if (array_key_exists($local_selector, $data)) {
          if ($local_property_name == "color") {
            $styles = $styles .' '. $data[$local_selector]['selector'] .'{color:#'. $property['value'] .' !important}';
          } else if ($local_property_name == "background_color") {
            $styles = $styles .' '. $data[$local_selector]['selector'] .'{background-color:#'. $property['value'] .' !important}';
          } else if ($local_property_name == "background_image") {
            $styles = $styles .' '. $data[$local_selector]['selector'] .'{background-image:url('. $property['value'] .') !important}';
          } else {
            $styles = $styles .' '. $data[$local_selector]['selector'] .'{'. str_replace('_', '-', $local_property_name) .':'. $property['value'] .' !important}';
          }
        }
      }
    }
  }
  return $styles;
}

function style_override_properties_form(&$form_state, $property = array()) {

  drupal_add_js(drupal_get_path('module', 'style_override') .'/colorpicker/js/colorpicker.js');
  drupal_add_js(drupal_get_path('module', 'style_override') .'/style_override_form.js');

  jquery_ui_add('ui.tabs');
  jquery_ui_add('ui.slider');

  $properties = style_override_property_load_all();
  $saved_values = array();
  if (is_array($properties)) {
    foreach ($properties as $property_id => $property) {
      $local_selector = $property['selector_id'];
      $local_property_name = $property['property'];
      $local_value = $property['value'];
      $saved_values[$local_selector][$local_property_name]['value'] = $local_value;
      $saved_values[$local_selector][$local_property_name]['property_id'] = $property_id;
    }
  }

  $selectors_by_rules = array();
  $rules = style_override_rule_load_all();
  foreach ($rules as $rule) {
    if (_style_override_evaluate_rule($rule)) {
      $selectors_by_rules[$rule['rule_id']] = style_override_selector_load_by_rule($rule['rule_id']);
      $tab_rule_list_items = $tab_rule_list_items .'<li><a href="#rule_'. $rule['rule_id'] .'">'. $rule['title'] .'</a></li>';
    }
  }

  $form['properties']['#tree'] = TRUE;

  $form['properties']['rules_wrapper_open'] = array(
    '#type' => 'markup',
    '#value' => '<div id="ui-rules-tabs" class="ui-tabs"><ul class="ui-rules-tabs">'. $tab_rule_list_items .'</ul>',
  );
  foreach ($selectors_by_rules as $rule_id => $selectors_by_rule) {
    $form['properties'][$rule_id .'wrapper_open'] = array(
      '#type' => 'markup',
      '#value' => '<div id="rule_'. $rule_id .'">',
    );
    $tab_selector_list_items = NULL;
    foreach ($selectors_by_rule as $selector_id => $selector) {
      $tab_selector_list_items = $tab_selector_list_items .'<li><a href="#selector_'. $selector['selector_id'] .'">'. $selector['title'] .'</a></li>';
    }
    $form['properties'][$rule_id .'_selector_tabs'] = array(
      '#type' => 'markup',
      '#value' => '<div id="ui-tabs" class="ui-tabs"><ul>'. $tab_selector_list_items .'</ul>',
    );
    foreach ($selectors_by_rule as $selector_id => $selector) {
      $form['properties'][$selector_id]['background_wrapper'] = array(
        '#type' => 'markup',
        '#value' => '<div id="selector_'. $selector['selector_id'] .'"><div class="fonts">',
      );
      $form['properties'][$selector_id]['font_size'] = array(
        '#type' => 'textfield',
        '#title' => t('Font size'),
        '#default_value' => $saved_values[$selector_id]['font_size']['value'],
        '#size' => '6',
        '#suffix' => '<div class="'. $selector_id .'_font_size_slider"></div>',
        '#attributes' => array('property' => 'font-size'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['font_size']['property_id'],
      );
      $form['properties'][$selector_id]['text_align'] = array(
        '#type' => 'Select',
        '#title' => t('text align'),
        '#options' => array('' => '', 'left' => 'left', 'right' => 'right', 'center' => 'center'),
        '#default_value' => $saved_values[$selector_id]['text_align']['value'],
        '#attributes' => array('property' => 'text-align'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['text_align']['property_id'],
      );
      $form['properties'][$selector_id]['font_family'] = array(
        '#type' => 'select',
        '#title' => t('Font family'),
        '#options' => array('' => '', 'Arial,Helvetica,sans-serif' => 'Arial, Helvetica, sans-serif', '"Times New Roman",Times,serif' => '"Times New Roman", Times, serif', '"Courier New",Courier,monospace' => '"Courier New", Courier, monospace'),
        '#default_value' => $saved_values[$selector_id]['font_family']['value'],
        '#attributes' => array('class' => 'font-family','property' => 'font-family'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['font_family']['property_id'],
      );
      $form['properties'][$selector_id]['line_height'] = array(
        '#type' => 'textfield',
        '#title' => t('Line height'),
        '#default_value' => $saved_values[$selector_id]['line_height']['value'],
        '#size' => '6',
        '#suffix' => '<div class="'. $selector_id .'_line_height_slider"></div>',
        '#attributes' => array('property' => 'line-height'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['line_height']['property_id'],
      );
      $form['properties'][$selector_id]['font_weight'] = array(
        '#type' => 'select',
        '#title' => t('Font weight'),
        '#options' => array('' => '', 'normal' => 'normal', 'bold' => 'bold'),
        '#default_value' => $saved_values[$selector_id]['font_weight']['value'],
        '#attributes' => array('property' => 'font-weight'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['font_weight']['property_id'],
      );
      $form['properties'][$selector_id]['text_transform'] = array(
        '#type' => 'select',
        '#title' => t('Transform'),
        '#options' => array('' => '', 'none' => 'none', 'uppercase' => 'uppercase', 'lowercase' => 'lowercase'),
        '#default_value' => $saved_values[$selector_id]['text_transform']['value'],
        '#attributes' => array('property' => 'text-transform'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['text_transform']['property_id'],
      );
      $form['properties'][$selector_id]['color'] = array(
        '#type' => 'textfield',
        '#title' => t('Color'),
        '#default_value' => $saved_values[$selector_id]['color']['value'],
        '#size' => '6',
        '#attributes' => array('property' => 'color'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['color']['property_id'],
      );
      $form['properties'][$selector_id]['fonts_wrapper'] = array(
        '#type' => 'markup',
        '#value' => '</div><div class="background">',
      );
      $form['properties'][$selector_id]['background_color'] = array(
        '#type' => 'textfield',
        '#title' => t('Background color'),
        '#default_value' => $saved_values[$selector_id]['background_color']['value'],
        '#size' => '6',
        '#attributes' => array('property' => 'background-color'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['background_color']['property_id'],
      );
      $form['properties'][$selector_id]['background_image'] = array(
        '#type' => 'textfield',
        '#title' => t('Background img'),
        '#default_value' => $saved_values[$selector_id]['background_image']['value'],
        '#size' => '12',
        '#attributes' => array('property' => 'background-image'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['background_image']['property_id'],
      );
      $form['properties'][$selector_id]['background_repeat'] = array(
        '#type' => 'select',
        '#title' => t('Background repeat'),
        '#default_value' => $saved_values[$selector_id]['background_repeat']['value'],
        '#options' => array('' => '', 'no-repeat' => 'no-repeat', 'repeat-x' => 'repeat-x', 'repeat-y' => 'repeat-y', 'repeat' => 'repeat'),
        '#attributes' => array('property' => 'background-repeat'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['background_repeat']['property_id'],
      );
      $form['properties'][$selector_id]['background_position'] = array(
        '#type' => 'select',
        '#title' => t('Background position'),
        '#default_value' => $saved_values[$selector_id]['background_position']['value'],
        '#options' => array('' => '', 'left top' => 'left top', 'left center' => 'left center', 'left bottom' => 'left bottom', 'center top' => 'center top', 'center center' => 'center center', 'center bottom' => 'center bottom', 'right top' => 'right top', 'right center' => 'right center', 'right bottom' => 'right bottom'),
        '#attributes' => array('property' => 'background-position'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['background_position']['property_id'],
      );
      $form['properties'][$selector_id]['display'] = array(
        '#type' => 'Select',
        '#title' => t('display'),
        '#default_value' => $saved_values[$selector_id]['display']['value'],
        '#options' => array('' => '', 'block' => 'block', 'inline' => 'inline', 'none' => 'none'),
        '#attributes' => array('property' => 'display'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['display']['property_id'],
      );
      $form['properties'][$selector_id]['height'] = array(
        '#type' => 'textfield',
        '#title' => t('Height'),
        '#default_value' => $saved_values[$selector_id]['height']['value'],
        '#size' => '6',
        '#suffix' => '<div class="'. $selector_id .'_height_slider"></div>',
        '#attributes' => array('property' => 'height'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['height']['property_id'],
      );
      $form['properties'][$selector_id]['width'] = array(
        '#type' => 'textfield',
        '#title' => t('Width'),
        '#default_value' => $saved_values[$selector_id]['width']['value'],
        '#size' => '6',
        '#suffix' => '<div class="'. $selector_id .'_width_slider"></div>',
        '#attributes' => array('property' => 'width'),
        '#selector_id' => $selector_id,
        '#property_id' => $saved_values[$selector_id]['width']['property_id'],
      );
      $form['properties'][$selector_id]['padding_wrapper'] = array(
        '#type' => 'markup',
        '#value' => '</div><div class="padding">',
      );
        $sides = array("top", "right", "bottom", "left");
      foreach ($sides as $side) {
        $form['properties'][$selector_id]['padding_'. $side] = array(
          '#type' => 'textfield',
          '#title' => t('Padding @side', array('@side' => $side)),
          '#default_value' => $saved_values[$selector_id]['padding_'. $side]['value'],
          '#size' => '6',
          '#suffix' => '<div class="'. $selector_id .'_padding_'. $side .'_slider"></div>',
          '#attributes' => array('property' => 'padding-'. $side),
          '#selector_id' => $selector_id,
          '#property_id' => $saved_values[$selector_id]['padding_'. $side]['property_id'],
        );
      }
      $form['properties'][$selector_id]['selector_wrapper_close'] = array(
        '#type' => 'markup',
        '#value' => '</div></div>',
      );
    }
    $form['properties'][$rule_id .'wrapper_close'] = array(
      '#type' => 'markup',
      '#value' => '</div></div>',
    );
  }
  $form['properties']['rules_wrapper_close'] = array(
    '#type' => 'markup',
    '#value' => '</div>',
  );
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * General submit handler for end-user style editing form.
 *
 * @ingroup formapi
 * @see style_override_form()
 */
function style_override_properties_form_submit($form, &$form_state) {
  $selectors = $form_state['values']['properties'];
  foreach ($selectors as $selector_id => $selector_value) {
    foreach ($selector_value as $property_name => $property_value) {
      $property_id = $form['properties'][$selector_id][$property_name]['#property_id'];
      if (!is_null($property_value)) {
        $save_item = new stdClass;
        $save_item->property_id = $property_id;
        $save_item->selector_id = $selector_id;
        $save_item->property = $property_name;
        $save_item->value = $property_value;
        style_override_property_save($save_item);
      }
      if (is_null($property_value)) {
        style_override_property_delete($property_id);
      }
    }
  }
}
